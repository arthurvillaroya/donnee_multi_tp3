#include <stdio.h>
#include "image_ppm.h"

OCTET median(OCTET* tab);
void tri(OCTET* tab, int taille);
void remplirMasque(int i, int j, int nW, OCTET* tab, OCTET* ImgIn);

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm Seuil \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   

   OCTET *ImgIn, *ImgOut, *MasqueTri;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(MasqueTri, OCTET, 9);
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
     {
      ImgOut[i*nW+j] = ImgIn[i*nW+j];
     }

 for (int i=1; i < nH - 1; i++)
   for (int j=1; j < nW - 1; j++)
     {
       remplirMasque(i,j,nW,MasqueTri,ImgIn);
       tri(MasqueTri, 9);
       ImgOut[i*nW+j] = median(MasqueTri); 
     }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}

void remplirMasque(int i, int j, int nW, OCTET* tab, OCTET* ImgIn){
  int cpt = 0;
  for(int x=-1; x < 2; x++){
    for(int y=-1; y < 2; y++){
      tab[cpt] = ImgIn[(i+x)*nW+(j+y)];
      cpt++;
    }
  }
}

OCTET median(OCTET* tab){
  return tab[4];
}

void tri(OCTET* tab, int taille){
  while(taille > 0){
    for (int i = 0; i < taille - 1; i++){
      if(tab[i] > tab[i+1]){
        int temp = tab[i];
        tab[i] = tab[i+1];
        tab[i+1] = temp;
      }
    }
    taille--;
  }
}