// test_couleur.cpp : Seuille une image en niveau de gris

// test_couleur.cpp : Seuille une image en niveau de gris

#include <iostream>
#include <stdio.h>
#include "image_ppm.h"
#include <cmath>

float distanceE(int centre, int x, int y);
OCTET nuance_moyenne_voisin(int x, int y, OCTET* ImgIn,  OCTET* Masque, int R, int nW);

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, R;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm Seuil \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d",&R);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

  OCTET *Masque;
  allocation_tableau(Masque, OCTET, (R+R+1)*(R+R+1));
  for (int i=0; i < R+R; i++){
    for (int j=0; j < R+R; j++){
      if(distanceE(R, i, j) <= R)
        Masque[i*(R+R)+j] = 1;
      else{
        Masque[i*(R+R)+j] = 0;
      }
    }
  }

   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
     {
      ImgOut[i*nW+j] = ImgIn[i*nW+j];
     }

  for (int i=R; i < nH - R; i++){
    for (int j=R; j < nW - R; j++){
      ImgOut[i*nW+j] = nuance_moyenne_voisin(i, j, ImgIn,  Masque, R, nW);
    }
  }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}

OCTET nuance_moyenne_voisin(int x, int y, OCTET* ImgIn,  OCTET* Masque, int R, int nW){
  int moy = 0;
  int xMasque = 0;
  int yMasque = 0;
  int nbocu = 0;
  for(int i = x - R; i <= x + R; i++){
    yMasque = 0;
    for(int j = y - R; j <= y + R; j++){
      if(Masque[xMasque*R+yMasque] != 0){
        moy = moy + (ImgIn[i*nW+j]* Masque[xMasque*R+yMasque]);
        nbocu++;
      } 
      yMasque++;
    }
    xMasque++;
  }
  return R == 0? ImgIn[x*nW+y]:moy/nbocu;
}


float distanceE(int centre, int x, int y){
  return sqrt((centre - x)*(centre - x) + (centre - y)*(centre - y));
}
