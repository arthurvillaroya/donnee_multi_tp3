// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"
#include <stdlib.h>
#include <cmath>

#define PI 3.141592

double distance(int x1, int y1, int x2, int y2);
double gauss(float a, double s);

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm Seuil \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

 for (int i=0; i < nH; i++)
   for (int j=0; j < nW; j++)
     {
      ImgOut[i*nW+j] = ImgIn[i*nW+j];
     }

 for (int i=1; i < nH - 1; i++)
   for (int j=1; j < nW - 1; j++)
     {
      double som = 0;
      double somCoef = 0;
      for(int x = -1; x <= 1; x++){
        for(int y = -1; y <= 1; y++){
          double gd = gauss(distance(i, j, (i+x), (j+y)),16);
          double gr = gauss(ImgIn[(i+x)*nW+(j+y)] - ImgIn[i*nW+j],12) ;
          double w = gd * gr;
          som =  som + ImgIn[(i+x)*nW+(j+y)] * w;
          somCoef = somCoef + w;
        }
      }
      /*printf("%f \n", somCoef);
      printf("%f\n", som/somCoef);
      printf("%d\n\n", ImgIn[i*nW+j]);*/
      if(somCoef > 0){
        ImgOut[i*nW+j] = som/somCoef;
      }
      else{
        ImgOut[i*nW+j] = ImgIn[i*nW+j];
      }
     }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

  return 1;
}

double distance(int x1, int y1, int x2, int y2){
  if(x1 != x2 && y1 != y2){
    return sqrt(2);
  }
  else if (x1 == x2 && y1 == y2){
    return 0;
  }
  else{
    return 1;
  }
}

double gauss(float x, double sigma) {
  return exp(-(pow(x, 2))/(2 * pow(sigma, 2))) / (2 * PI * pow(sigma, 2));
}