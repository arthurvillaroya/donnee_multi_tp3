// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"
#include <cmath>

OCTET nuance_moyenne_voisin(int x, int y, OCTET* ImgIn,  float* Masque, int R, int nW);
float coef(int centre, int x, int y);

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, R;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm Seuil \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d",&R);
   

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

  float *Masque;
  allocation_tableau(Masque, float, (R+R+1)*(R+R+1));
  for (int i=0; i < R+R; i++){
    for (int j=0; j < R+R; j++){
      float a = coef(R, i, j);
      Masque[i*R+j] = a;
    }
  }

   for (int i=0; i < nH; i++)
    for (int j=0; j < nW; j++)
     {
      ImgOut[i*nW+j] = ImgIn[i*nW+j];
     }
  for (int i=1; i < nH - 1; i++){
    for (int j=1; j < nW - 1; j++){
      ImgOut[i*nW+j] = nuance_moyenne_voisin(i, j, ImgIn,  Masque, R, nW);
    }
  }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}

OCTET nuance_moyenne_voisin(int x, int y, OCTET* ImgIn,  float* Masque, int R, int nW){
  int moy = 0;
  int xMasque = 0;
  int yMasque = 0;
  float div = 0;
  for(int i = x - R; i <= x + R; i++){
    yMasque = 0;
    for(int j = y - R; j <= y + R; j++){
        moy = moy + (ImgIn[i*nW+j] * Masque[xMasque*R+yMasque]);
        div = div + Masque[xMasque*R+yMasque];
      yMasque++;
    }
    xMasque++;
  }
  return div == 0? ImgIn[x*nW+y]:moy/div;
}

float distanceE(int centre, int x, int y){
  return sqrt((centre - x)*(centre - x) + (centre - y)*(centre - y));
}

float coef(int centre, int x, int y){
  if(distanceE(centre, x, y) > centre){
    return 0;
  }
  else{
    return (float)centre - distanceE(centre, x, y);
  }
}
