#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, indice;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm indice\n"); 
       exit (1) ;
     }
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%d", &indice);
   

   OCTET *ImgIn;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
	
    int T[nW][2];
    for(int i=0; i<nW; i++)
    {
      T[i][0]=i;
      T[i][1]=0;
    } 

    for (int j=0; j < nW; j++)
    {
      T[j][1] = ImgIn[indice*nW+j];
    }

    std::ofstream ofs ("profil5.dat", std::ofstream::out);

    for(int i=0; i < nW; i++)
    {
      ofs<<T[i][0]<<"  "<<T[i][1]<<"\n";
    }

    ofs.close();
   return 1;
}