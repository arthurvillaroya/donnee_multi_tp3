// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"
#include <stdlib.h>

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm Seuil \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

 for (int i=0; i < nH; i++)
   for (int j=0; j < nW; j++)
     {
      ImgOut[i*nW+j] = ImgIn[i*nW+j];
     }

 for (int i=1; i < nH - 1; i++)
   for (int j=1; j < nW - 1; j++)
     {
      int som = 0; //0
      int somCoef = 1;
      for(int x = -1; x <= 1; x++){
        for(int y = -1; y <= 1; y++){
          som += ImgIn[(i+x)*nW+(j+y)] * abs(ImgIn[(i+x)*nW+(j+y)] - ImgIn[i*nW+j]);
          somCoef += abs(ImgIn[(i+x)*nW+(j+y)] - ImgIn[i*nW+j]);
        }
      }
      ImgOut[i*nW+j] = som/somCoef;
     }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

  return 1;
}
